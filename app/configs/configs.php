<?php

return [
  'url' => '/git/booking/',

  'templates' => ROOT . 'views',

  'debug_log' => LOGS . 'app.log',

  'db.host' => 'localhost',

  'db.username' => 'root',

  'db.password' => '',

  'db.database' => 'booking'

];

?>