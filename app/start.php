<?

# https://github.com/s-melnikov/dispatch-2.0
require LIBS . 'dispatch.php';

# additional functions
require APP . 'functions.php';

# configs
config(require CONFIGS . 'configs.php');

# app routes
require APP . 'routes.php';

# connect to database
$connect = mysqli_connect(
  config('db.host'),
  config('db.username'),
  config('db.password'),
  config('db.database')
);

dispatch($connect);