<?

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath(dirname(__FILE__)) . DS);
define('APP', ROOT . 'app' . DS);
define('LIBS', APP . 'libs' . DS);
define('CONFIGS', APP . 'configs' . DS);
define('LOGS', APP . 'logs' . DS);

require APP . 'start.php';