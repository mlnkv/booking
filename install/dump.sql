DROP DATABASE IF EXISTS `{{dbname}}`;

CREATE DATABASE `{{dbname}}`;

USE `{{dbname}}`;

CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(140) NOT NULL,
  `phonenumber` varchar(32) NOT NULL,
  `status` enum('inactive','active') DEFAULT 'active',
  `type` enum('superuser', 'administrator','editor','user') NOT NULL,
  `hash` varchar(64) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARSET = utf8;

INSERT INTO `users` (`login`, `firstname`, `lastname`, `birthday`, `email`, `phonenumber`, `type`, `hash`) VALUES
('superuser', 'Джеф', 'Армстронг', '1971-8-12', 'jeff.armstrong12@example.com', '(808)-499-6292', 'superuser', 'e0bc60c82713f64ef8a57c0c40d02ce24fd0141d5cc3086259c19b1e62a62bea'),
('leta.black', 'Лета', 'Блэк', '1970-12-23', 'leta.black12@example.com', '(583)-279-3776', 'administrator', 'e0bc60c82713f64ef8a57c0c40d02ce24fd0141d5cc3086259c19b1e62a62bea'),
('shelly.hopkins', 'Шелли', 'Хопкинс', '1974-9-19', 'shelly.hopkins22@example.com', '(407)-686-5758', 'editor', 'e0bc60c82713f64ef8a57c0c40d02ce24fd0141d5cc3086259c19b1e62a62bea'),
('allison.ryan', 'Эллисон', 'Раен', '1983-7-8', 'allison.ryan37@example.com', '(900)-487-6041', 'editor', 'e0bc60c82713f64ef8a57c0c40d02ce24fd0141d5cc3086259c19b1e62a62bea'),
('jim.clark', 'Джим', 'Кларк', '1979-1-20', 'jim.clark92@example.com', '(464)-949-9206', 'user', 'e0bc60c82713f64ef8a57c0c40d02ce24fd0141d5cc3086259c19b1e62a62bea'),
('jordan.roberts', 'Джордан', 'Робертс', '1976-9-16', 'jordan.roberts85@example.com', '(571)-838-8238', 'user', 'e0bc60c82713f64ef8a57c0c40d02ce24fd0141d5cc3086259c19b1e62a62bea'),
('adrian.thomas', 'Адриан', 'Томас', '1985-7-10', 'adrian.thomas77@example.com', '(786)-810-8778', 'user', 'e0bc60c82713f64ef8a57c0c40d02ce24fd0141d5cc3086259c19b1e62a62bea');
