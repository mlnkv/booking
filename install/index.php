<?php

$host = 'localhost';
$username = 'root';
$password = '';
$database = 'booking';

$conn = new mysqli($host, $username, $password);

if ($conn->connect_error) {
  exit('Connection failed: ' . $conn->connect_error);
}

$sql = file_get_contents('dump.sql');
$sql = preg_replace('/{{dbname}}/', $database, $sql);

if ($conn->multi_query($sql)) {
  echo 'Database created successfully';
} else {
  echo 'Error multi query: ' . $conn->error;
}

$conn->close();

?>